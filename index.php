<?php
/**
 * Created by PhpStorm.
 * User: marwen
 * Date: 21/07/19
 * Time: 21:11
 */

include("src/api/rest.php");

$tasks = new rest();

//Count input params
$count_param = count($argv);
// Execution with one param
if ($count_param ==2 && isset($argv[1])) {
	//Call rest api
	$get_data = $tasks->callAPI('GET', 'https://restcountries.eu/rest/v2/name/'.$argv[1].'/', false);
	$response = json_decode($get_data, true);
	$lang = $response[0]['languages'][0]['iso639_1'];
	echo($lang);
	echo "\n";

	//Output message to concatenate
	$chaine =  $argv[1]. " speaks same language with these countries: ";

	//Call rest api
	$get_data_by_lang = $tasks->callAPI('GET', 'https://restcountries.eu/rest/v2/lang/'.$lang.'/', false);
	$countries_by_lang = json_decode($get_data_by_lang, true);

	//list countries
	$list_country = null;
	for($i = 0; $i<=count($countries_by_lang)-1; $i++){
		//filter existant country
		if($argv[1] != $list_country.$countries_by_lang[$i]['name']){
			$list_country= $list_country.$countries_by_lang[$i]['name']. ', ';
		}
	}
	//Output message
	echo $chaine. $list_country ."\n";

}
//execution with two params
elseif ($count_param ==3 && isset($argv[1]) && isset($argv[2])){

	//initialisation
	$languages = [];
	$countries = null;
	//Call rest api
	for($i = 1 ; $i<=2; $i++){
		$get_data = $tasks->callAPI('GET', 'https://restcountries.eu/rest/v2/name/'.$argv[$i].'/', false);
		$response = json_decode($get_data, true);

		array_push($languages, $response[0]['languages'][0]['iso639_1']);
	}
	//Compare two languages
	if($languages[0] === $languages[1]){
	   echo $argv[1]. " and ". $argv[2] ." speak the same language \n";
	} else{
		echo $argv[1]. " and ". $argv[2] ." do not speak the same language \n";
	}
}
else {
	echo "argc and argv disabled \n";
}

?>
